Ntrvr.GoalsController = Ember.ArrayController.extend({
    actions: {
        createGoal: function() {
            // Get the goal title set by the "New Goal" text field
            var title = this.get('newTitle');

            if (!title) {
                return false;
            }
            if (!title.trim()) {
                return;
            }

            // Create the new Goal model
            var goal = this.store.createRecord('goal', {
                title: title,
                isDone: false
            });

            // Clear the "New Goal" text field
            this.set('newTitle', '');

            // Save the new model
            goal.save();
        }
    }
});


Ntrvr.GoalController = Ember.ObjectController.extend({
    isDone: function(key, value) {
        var model = this.get('model');

        if (value === undefined) {
            // property being used as a getter
            return model.get('isDone');
        } else {
            // property being used as a setter
            model.set('isDone', value);
            model.save();
            return value;
        }
    }.property('model.isDone')
});
Ntrvr.Goal = DS.Model.extend({
    title: DS.attr('string'),
    isDone: DS.attr('boolean')
});

Ntrvr.Goal.FIXTURES = [{
    id: 1,
    title: "I have to validate email and confirm email (email validation code and confirmation?)",
    isDone: true
}, {
    id: 2,
    title: "Display list of interviewers",
    isDone: false
}, {
    id: 3,
    title: "Be able to select them",
    isDone: false
}, {
    id: 4,
    title: "It should redirect to detail view",
    isDone: false
}, {
    id: 5,
    title: "(?) Schedule a candidate for interview",
    isDone: false
}, {
    id: 6,
    title: '(?) Responsd "Thanks"',
    idDone: false
}];
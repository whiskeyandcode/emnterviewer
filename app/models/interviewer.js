Ntrvr.Interviewer = DS.Model.extend({
    name: DS.attr('string'),
    emai: DS.attr('string')
});

Ntrvr.Interviewer.FIXTURES = [{
    id: 1,
    name: "Prashanth",
    email: "ppadmanabh@example.com"
}, {
    id: 2,
    name: "George Washington",
    email: "gwashington@example.com"
}, {
    id: 3,
    name: "Thomas Jefferson",
    email: "tjefferson@example.com"
}];
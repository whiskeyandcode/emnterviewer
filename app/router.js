Ntrvr.Router.map(function() {
    this.resource('goals', {
        path: '/'
    });
});


Ntrvr.GoalsRoute = Ember.Route.extend({
    model: function() {
        return this.store.find('goal');
    }
});